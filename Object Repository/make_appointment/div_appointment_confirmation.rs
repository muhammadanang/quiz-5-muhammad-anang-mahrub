<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_appointment_confirmation</name>
   <tag></tag>
   <elementGuidId>fe3ebaca-7c1e-4d12-863a-054144601b73</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='summary']/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.row</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>2639f88f-9fc0-44fc-bb00-9b23931ff4a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row</value>
      <webElementGuid>8f9f6dca-f3ab-4450-a48d-b59c413f8ff0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
                Appointment Confirmation
                Please be informed that your appointment has been booked as following:
                
            
            
                
                    Facility
                
                
                    Tokyo CURA Healthcare Center
                
            
            
                
                    Apply for hospital readmission
                
                
                    No
                
            
            
                
                    Healthcare Program
                
                
                    Medicare
                
            
            
                
                    Visit Date
                
                
                    23/11/2022
                
            
            
                
                    Comment
                
                
                    
                
            
            
                Go to Homepage
            
        </value>
      <webElementGuid>2e42f086-18d0-4fd3-a255-7a9528599665</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;summary&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]</value>
      <webElementGuid>17108828-2d2b-417e-a6ef-3f3bb42e783b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='summary']/div/div</value>
      <webElementGuid>b28d7d2c-c66d-4cc7-9dfb-7b7b976bcf8f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[1]/following::div[2]</value>
      <webElementGuid>7584a34a-d815-4118-93f2-ff86daf92fe7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='We Care About Your Health'])[1]/following::div[2]</value>
      <webElementGuid>347e99bb-7944-42a8-81b3-d1a1c1bb97fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div</value>
      <webElementGuid>27333344-82db-44df-8322-aa48473142f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            
                Appointment Confirmation
                Please be informed that your appointment has been booked as following:
                
            
            
                
                    Facility
                
                
                    Tokyo CURA Healthcare Center
                
            
            
                
                    Apply for hospital readmission
                
                
                    No
                
            
            
                
                    Healthcare Program
                
                
                    Medicare
                
            
            
                
                    Visit Date
                
                
                    23/11/2022
                
            
            
                
                    Comment
                
                
                    
                
            
            
                Go to Homepage
            
        ' or . = '
            
                Appointment Confirmation
                Please be informed that your appointment has been booked as following:
                
            
            
                
                    Facility
                
                
                    Tokyo CURA Healthcare Center
                
            
            
                
                    Apply for hospital readmission
                
                
                    No
                
            
            
                
                    Healthcare Program
                
                
                    Medicare
                
            
            
                
                    Visit Date
                
                
                    23/11/2022
                
            
            
                
                    Comment
                
                
                    
                
            
            
                Go to Homepage
            
        ')]</value>
      <webElementGuid>7f042e71-1e8a-4ab6-83b2-14ab6ac34d3f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
