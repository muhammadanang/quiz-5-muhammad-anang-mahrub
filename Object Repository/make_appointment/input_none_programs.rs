<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_none_programs</name>
   <tag></tag>
   <elementGuidId>4d7e52c9-d9f3-4fe1-818c-6c8907016271</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='radio_program_none']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#radio_program_none</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>a624060e-55ed-4552-be03-405c226633ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>98e1a670-6e43-4d48-9b70-31486a14a02c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>programs</value>
      <webElementGuid>fa8eac71-ce2f-457c-919d-9846a10c77d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>radio_program_none</value>
      <webElementGuid>45304438-414e-411f-a13c-203a2a1259fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>None</value>
      <webElementGuid>27ee1bb0-5945-4a00-a2be-7439edce454b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;radio_program_none&quot;)</value>
      <webElementGuid>e1137fa0-73c8-4393-9868-2b748274dd17</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='radio_program_none']</value>
      <webElementGuid>142196dc-c4bc-4107-89b8-c2fa10d8e586</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[3]/div/label[3]/input</value>
      <webElementGuid>278545f8-dbf8-45f6-884f-7d22b6e2eda0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label[3]/input</value>
      <webElementGuid>e024c246-e79e-4d6e-be43-78f4a6c6fe49</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'radio' and @name = 'programs' and @id = 'radio_program_none']</value>
      <webElementGuid>13cd5228-70c1-417c-834a-f0924b0ea468</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
