import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

def now = new Date().format('dd/MM/yyyy')

WebUI.openBrowser('https://katalon-demo-cura.herokuapp.com/')

WebUI.click(findTestObject('menu/btn_menu_toggle'))

WebUI.click(findTestObject('menu/btn_menu_login'))

WebUI.setText(findTestObject('login/input_username'), 'John Doe')

WebUI.setText(findTestObject('login/input_password'), 'ThisIsNotAPassword')

WebUI.click(findTestObject('login/btn_Login'))

WebUI.click(findTestObject('make_appointment/btn_make_appointment'))

WebUI.selectOptionByValue(findTestObject('make_appointment/select_facility'), 'Seoul CURA Healthcare Center', false)

WebUI.check(findTestObject('make_appointment/input_readmission'))

WebUI.check(findTestObject('make_appointment/input_medicaid_programs'))

WebUI.setText(findTestObject('make_appointment/input_visit_date'), 'hello world')

WebUI.setText(findTestObject('make_appointment/textarea_comment'), 'It\'s a comment')

WebUI.click(findTestObject('make_appointment/btn_booking'))

WebUI.verifyElementText(findTestObject('make_appointment/p_visit_date'), now.toString())

WebUI.verifyElementText(findTestObject('make_appointment/p_booking_message'), 'Please be informed that your appointment has been booked as following:')

WebUI.closeBrowser()

